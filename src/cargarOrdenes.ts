import { readFileSync } from 'fs'
import { Resultado } from './Resultado'
import { Orden } from './Orden'

export function cargarOrdenes(path: string): Resultado<Orden[], string> {
  const datos = JSON.parse(readFileSync(path, 'utf8'))

  if ('ordenes' in datos) {
    const ordenes: Orden[] = datos['ordenes']
    return { error: false, valor: ordenes }
  } else {
    return { error: true, valor: 'El archivo no es un archivo de ordenes válido.' }
  }
}