import { PaginaSolicitudReal } from "./paginas/PaginaSolicitudReal";
import { PaginaResultadoReal } from "./paginas/PaginaResultadoReal";
import { crearDriver } from "./crearDriver";
import { Orden } from "./Orden";
import { cargarOrdenes } from "./cargarOrdenes";
import * as commander from "commander";
import { PaginaResultadoDebug } from "./paginas/PaginaResultadoDebug";
import { PaginaSolicitudDebug } from "./paginas/PaginaSolicitudDebug";
import { PaginaSolicitud } from "./paginas/PaginaSolicitud";
import { PaginaResultado } from "./paginas/PaginaResultado";

type Opciones = { mock?: boolean, path: string };

const parser = crearParserOpciones()

const opciones: Opciones = parser.parse(process.argv) as any;

main(opciones);

async function main(opciones: Opciones) {
  // intentar conseguir ordenes
  const maybeOrdenes = cargarOrdenes(opciones.path);

  if (maybeOrdenes.error == false) {
    const ordenes = maybeOrdenes.valor;

    if (opciones.mock) {
      const paginaSolicitud = new PaginaSolicitudDebug()
      const paginaResultado = new PaginaResultadoDebug()
      paginaSolicitud.esperarNavegacion()
      procesarOrdenes(ordenes, paginaSolicitud, paginaResultado)
    } else {
      // Crear driver
      const driver = crearDriver();

      const paginaSolicitud = new PaginaSolicitudReal(driver);

      await driver.get(
        "https://www.traditum.com/NEO%20TDM%20Canal%20IT/View/Login.aspx"
      );
      
      const paginaResultado = new PaginaResultadoReal(driver);

      console.log(`Iniciando procesamiendo de ${ordenes.length} orden/es.`)

      procesarOrdenes(ordenes, paginaSolicitud, paginaResultado);
    }
  } else {
    throw new Error(maybeOrdenes.valor);
  }
}

async function procesarOrdenes(
  ordenes: Orden[],
  paginaSolicitud: PaginaSolicitud,
  paginaResultado: PaginaResultado
) {
  await paginaSolicitud.esperarNavegacion();

  const total = ordenes.length;

  for (let [indice, orden] of ordenes.entries()) {
    console.log(`Procesando orden ${indice + 1} de ${total}`);
    await paginaSolicitud.ingresar();
    await paginaSolicitud.rellenarAfiliado(orden.afiliado);
    await paginaSolicitud.buscarAfiliado();
    // TO DO: verificar que el afiliado ha sido ingresado correctamente
    for (let prestacion of orden.prestaciones) {
      await paginaSolicitud.agregarPrestacion(prestacion);
      // TO DO: verificar que la prestacion ha sido agregada correctamente
    }
    await paginaSolicitud.enviarSolicitud();
    await paginaSolicitud.esperarResultado();
    await paginaResultado.exportarSolicitud();
    await paginaResultado.volver();

    // Esperar en las ordenes con indice multiplo de 10
    if (indice % 10 == 0) {
      console.log("Haciendo tiempo antes de continuar...")
      await new Promise(resolve => setTimeout(resolve, 10 * 1000))
    }
  }
}

function crearParserOpciones() {
  return commander
  .version("0.0.1")
  .option(
    "-M, --mock",
    "Ejecuta en modo 'imitación' (se reportan las acciones que se tomaría, sin realizarlas)."
  )
  .option(
    "-P, --path <archivo>",
    "Camino al archivo de donde se leeran las ordenes."
  )
}