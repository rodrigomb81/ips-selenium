import { WebDriver, Builder } from "selenium-webdriver";
import { Options } from "selenium-webdriver/firefox";

export function crearDriver(): WebDriver {
  const options = new Options();
  options.addArguments("--user-data-dir=/home/rodrigo/.config/google-chrome");
  return new Builder()
    .forBrowser('chrome')
    .build();
}