export type Resultado<A, B> = Exito<A> | Falla<B>

type Exito<A> = {
  error: false
  valor: A
}

type Falla<B> = {
  error: true
  valor: B
}