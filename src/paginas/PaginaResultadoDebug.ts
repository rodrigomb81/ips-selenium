import { PaginaResultado } from "./PaginaResultado";

export class PaginaResultadoDebug implements PaginaResultado {
  async exportarSolicitud() {
    console.log("Exportando solicitud...")
  }

  async ingresar() {
    console.log(`Ingresando a frame "myFrame"...`)
  }

  async volver() {
    console.log("Volviendo a página de autorizacion...")
  }
}