import { PaginaSolicitud } from "./PaginaSolicitud";

export class PaginaSolicitudDebug implements PaginaSolicitud {
  async esperarNavegacion() {
    console.log("Esperando a que el usuario haga el log in y navegue a 'Solicitud de autorizacion'...")
  }

  async ingresar() {
    console.log("Ingresando al iframe de controles...")
  }

  async rellenarAfiliado(a: string) {
    console.log(`Rellenando campo afiliado con número ${a}`)
  }

  async buscarAfiliado() {
    console.log('Buscando afiliado...')
  }

  async agregarPrestacion(c: string) {
    console.log(`Agregando prestación con código ${c}`)
  }

  async enviarSolicitud() {
    console.log("Enviando solicitud...")
  }

  async esperarResultado() {
    console.log("Esperando resultado (exportando archivo)")
  }
}