import { WebDriver, By } from "selenium-webdriver";
import { PaginaResultado } from "./PaginaResultado";

export class PaginaResultadoReal implements PaginaResultado {
  private driver: WebDriver;

  constructor(d: WebDriver) {
    this.driver = d;
  }

  async exportarSolicitud() {
    const boton = await this.driver.findElement(By.id("bodyContent_btnExportar"))

    await boton.click()
  }

  async ingresar() {
    const iframe = await this.driver.findElement(By.id("myIframe"));
    await this.driver.switchTo().frame(iframe);
  }

  async volver() {
    const tabs = await this.driver.getAllWindowHandles()

    await this.driver.switchTo().window(tabs[0])

    await this.driver.navigate().back()

    await this.driver.navigate().back()
  }
}