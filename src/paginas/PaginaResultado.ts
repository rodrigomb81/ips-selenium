export interface PaginaResultado {
  exportarSolicitud(): Promise<void>

  ingresar(): Promise<void>

  volver(): Promise<void>
}