import { WebDriver, By, until, Key } from "selenium-webdriver";
import { PaginaSolicitud } from "./PaginaSolicitud";

export class PaginaSolicitudReal implements PaginaSolicitud {
  private driver: WebDriver;

  constructor(d: WebDriver) {
    this.driver = d;
  }

  /**
   * Espera como mucho 30s hasta que el usuario navegue hasta
   * la pagina de Solicitud de Autorizacion
   */
  async esperarNavegacion() {
    await this.driver
      .wait(until.elementLocated(By.id("myIframe")), 30 * 1000)
      .catch(() => {
        console.log("Hubo un error al navegar a 'Solicitud de autorizacion'");
      });

    await this.driver
      .wait(until.elementLocated(By.id("bodyContent_spnTituloMensaje")), 30 * 1000)
      .catch(() => {
        console.log("Hubo un error al navegar a 'Solicitud de autorizacion'");
      });
  }

  /**
   * Ingresa al iframe que contiene los controles de esta pagina
   */
  async ingresar() {
    const iframe = await this.driver.findElement(By.id("myIframe"));
    await this.driver.switchTo().frame(iframe);
  }

  async rellenarAfiliado(afiliado: string) {
    const campo = await this.driver.findElement(
      By.id("bodyContent_PID_txtNroIDAfiliado")
    );

    await campo.sendKeys(afiliado);
  }

  async buscarAfiliado() {
    const boton = await this.driver.findElement(
      By.id("bodyContent_PID_btnNroIDAfiliado_UniqueSearch")
    );

    await boton.click();

    /**
     * Esperar hasta que el afiliado sea encontrado
     */
    await this.driver.wait(
      until.elementIsNotVisible(
        await this.driver.findElement(By.id("divLoading"))
      )
    );
  }

  // /**
  //  * TODO: Verificar que el afiliado ha sido correctamente cargado
  //  */
  // async verificarAfiliado() {}

  async agregarPrestacion(codigo: string) {
    const campo = await this.driver.findElement(
      By.id("bodyContent_PRE_txtCodigoPrestacion")
    );

    await campo.sendKeys(codigo);

    campo.sendKeys(Key.ENTER);

    /**
     * Esperar hasta que la prestacion sea agregada
     */
    await this.driver.wait(
      until.elementIsNotVisible(
        await this.driver.findElement(By.id("divLoading"))
      )
    );

    // El codigo original tiene, en esta parte, un implicit wait de 2 segundos. Tenerlo en cuenta.
  }

  async enviarSolicitud() {
    const boton = await this.driver.findElement(
      By.id("bodyContent_btnAceptar")
    );

    await boton.click();

    /**
     * Esperar hasta que la solicitud sea enviada
     */
    await this.driver.wait(
      until.elementIsNotVisible(
        await this.driver.findElement(By.id("divLoading"))
      )
    );
  }

  async esperarResultado() {
    await this.driver.wait(
      until.elementIsNotVisible(
        await this.driver.findElement(By.id("divLoading"))
      )
    );
  }
}
