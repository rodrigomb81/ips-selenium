export interface PaginaSolicitud {
  esperarNavegacion(): Promise<void>

  ingresar(): Promise<void>

  rellenarAfiliado(a: string): Promise<void>

  buscarAfiliado(): Promise<void>

  agregarPrestacion(c: string): Promise<void>

  enviarSolicitud(): Promise<void>

  esperarResultado(): Promise<void>
}