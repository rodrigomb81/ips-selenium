import {
  Builder,
  By,
  until,
  Key,
  ThenableWebDriver,
  WebElement
} from "selenium-webdriver";
import { Options } from "selenium-webdriver/chrome";

type orden = { afiliado: string, prestaciones: string[] }

async function login(driver: ThenableWebDriver) {
  /**
   * Hacer login:
   *  1 Encontrar inputs
   *  2 Introducir credenciales
   *  3 Hacer click en anchor login (inicia cambio de contenido de la pagina)
   *  4 Verificar que la navegacion terminó buscando un elemento especifico
   */

  // 1
  const campoUsuario = driver.findElement(By.id("txtUsuario"));
  const campoPass = driver.findElement(By.id("txtPassword"));
  const anchorLogin = driver.findElement(By.id("btnLogin"));

  await Promise.all([campoUsuario, campoPass, anchorLogin]);

  // 2
  await Promise.all([
    campoUsuario.sendKeys("IT011488"),
    campoPass.sendKeys("09wtS10s")
  ]);

  // 3
  await anchorLogin.click();

  // 4
  await driver.wait(
    until.elementLocated(By.id("lblUsuarioLogo")),
    10000,
    "Hubo un error durante el login"
  );
}

async function ingresarIFrame(driver: ThenableWebDriver) {
  const myIframe = await driver.findElement(By.id("myIframe"));
  await driver.switchTo().frame(myIframe);
}

async function rellenarNroAfiliado(
  driver: ThenableWebDriver,
  campo: WebElement,
  botonBuscarAfiliado: WebElement,
  nro: string,
  campoNombreAfiliado: WebElement
) {
  await campo.sendKeys(nro);
  await botonBuscarAfiliado.click();
  driver.wait(
    until.elementTextMatches(campoNombreAfiliado, /./),
    5000,
    "Hubo un problema al buscar el nombre del afiliado"
  );
}

async function rellenarPrestacion(driver: ThenableWebDriver, campoCodigoPrestacion: WebElement, codigoPrestacion: string) {
  await campoCodigoPrestacion.sendKeys(codigoPrestacion);
  await campoCodigoPrestacion.sendKeys(Key.ENTER);
  await driver.wait(
    until.elementTextIs(campoCodigoPrestacion, ""),
    5000,
    "Hubo un problema al rellenar prestacion"
  );
}

async function verificarPrestacionCargada(driver: ThenableWebDriver) {
  const divLoading = await driver.findElement(By.id("divLoading"));
  await driver.wait(until.elementIsNotVisible(divLoading));
  const esperarDosSegs = new Promise(resolve => setTimeout(resolve, 2000));
  await driver.wait(esperarDosSegs);
}

async function test() {
  const options = new Options();
  options.addArguments("--user-data-dir=/home/rodrigo/.config/google-chrome");
  const driver = new Builder()
    .forBrowser("chrome")
    .setChromeOptions(options)
    .build();

  // Navegar a pagina de login
  await driver.get(
    "https://www.traditum.com/NEO%20TDM%20Canal%20IT/View/Login.aspx"
  );

  await login(driver);

  // Verificar que me encuentro en apartado "Solicitud de autoriazacion"
  await driver
    .wait(until.elementLocated(By.id("myIframe")), 10000)
    .catch(() => {
      console.log("Hubo un error al navegar a 'Solicitud de autorizacion'");
    });
  
  await new Promise((resolve => setTimeout(resolve, 4000)))

  
  async function procesarOrdenes(ordenes: orden[]) {
    // Los elementos que necesito estan en un iframe. Necesito cambiar el contexto a el.
    await ingresarIFrame(driver);

    // Obtener controles
    const campoNroAfiliado = driver.findElement(
      By.id("bodyContent_PID_txtNroIDAfiliado")
    );
    const campoNombreAfiliado = driver.findElement(
      By.id("bodyContent_PID_txtApellidoAfiliado")
    );
    const botonBuscarAfiliado = driver.findElement(
      By.id("bodyContent_PID_btnNroIDAfiliado_UniqueSearch")
    );
    const campoCodigoPrestacion = driver.findElement(
      By.id("bodyContent_PRE_txtCodigoPrestacion")
    );
    const botonEnviarSolicitud = driver.findElement(
      By.id("bodyContent_btnAceptar")
    );

    await Promise.all([
      campoNroAfiliado,
      campoNombreAfiliado,
      botonBuscarAfiliado,
      campoCodigoPrestacion,
      botonEnviarSolicitud
    ]);

    async function procesarOrden(unaOrden: orden) {
      rellenarNroAfiliado(driver, campoNombreAfiliado, botonBuscarAfiliado, unaOrden.afiliado, campoNombreAfiliado)
      for (let i = 0; i < unaOrden.prestaciones.length; i++) {
        rellenarPrestacion(driver, campoCodigoPrestacion, unaOrden.prestaciones[i])
        await verificarPrestacionCargada(driver)
      }
    }

    for (let i = 0; i < ordenes.length; i++) {
      await procesarOrden(ordenes[i])
      await botonEnviarSolicitud.click();
    }

    await driver.wait(until.elementLocated(By.id("bodyContent_btnExportar")));
  
    const botonExportar = await driver.findElement(
      By.id("bodyContent_btnExportar")
    );
  
    await botonExportar.click();
  
    await driver.wait(
      until.elementLocated(By.id("bodyContent_btnVolver")),
      10000,
      "Hubo un problema al descargar el archivo"
    );
  
    await driver.navigate().back();

    await driver.wait(until.elementLocated(By.id("myIframe")));
  }

  const ordenes: orden[] = [
    {
      afiliado: "003626831600", prestaciones: ["425031"],
    },
    {
      afiliado: "002297771200", prestaciones: ["425031"]
    }
  ]

  await procesarOrdenes(ordenes)

  // // Los elementos que necesito estan en un iframe. Necesito cambiar el contexto a el.
  // // await ingresarIFrame(driver);
  // const myIframe = await driver.findElement(By.id("myIframe"));
  // await driver.switchTo().frame(myIframe);

  // // Obtener controles
  // const campoNroAfiliado = driver.findElement(
  //   By.id("bodyContent_PID_txtNroIDAfiliado")
  // );
  // const campoNombreAfiliado = driver.findElement(
  //   By.id("bodyContent_PID_txtApellidoAfiliado")
  // );
  // const botonBuscarAfiliado = driver.findElement(
  //   By.id("bodyContent_PID_btnNroIDAfiliado_UniqueSearch")
  // );
  // const campoCodigoPrestacion = driver.findElement(
  //   By.id("bodyContent_PRE_txtCodigoPrestacion")
  // );
  // const botonEnviarSolicitud = driver.findElement(
  //   By.id("bodyContent_btnAceptar")
  // );
  // // const botonAgregarPrestacion = driver.findElement(By.id("bodyContent_PRE_btnTablaPrestaciones_Add"))

  // await Promise.all([
  //   campoNroAfiliado,
  //   campoNombreAfiliado,
  //   botonBuscarAfiliado,
  //   campoCodigoPrestacion,
  //   botonEnviarSolicitud
  // ]);

  // // Rellenar campo nro afiliado
  // await rellenarNroAfiliado(driver, campoNroAfiliado, botonBuscarAfiliado, "011798019104", campoNombreAfiliado)

  // await rellenarPrestacion(driver, campoCodigoPrestacion, "425031")

  // await verificarPrestacionCargada(driver)

  // botonEnviarSolicitud.click();

  // await driver.wait(until.elementLocated(By.id("bodyContent_btnExportar")));

  // const botonExportar = await driver.findElement(
  //   By.id("bodyContent_btnExportar")
  // );

  // await botonExportar.click();

  // await driver.wait(
  //   until.elementLocated(By.id("bodyContent_btnVolver")),
  //   10000,
  //   "Hubo un problema al descargar el archivo"
  // );

  // await driver.navigate().back();

  // await driver.wait(
  //   until.elementLocated(By.id("bodyContent_PID_txtNroIDAfiliado")),
  //   10000,
  //   "Hubo un error volver a 'Solicitud de autorizacion'"
  // );

  driver.quit();
}

test();
